﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using UnityEngine.UI;

public class Network : MonoBehaviour
{
    //************
    //Logic Variables
    //************
    public GameObject Input_UI;
    public GameObject Saved_UI;

    public Text status;

    //************
    //Classes
    //************
    Game game_script;
    UI ui_script;


    //************
    //Methods
    //************

    //************
    //Requesting Game from Server
    //************

    //trigger manual update
    public void update_load()
    {
        ui_script.Status("Updating game.");
        Request(Format_Request(game_script.Get()));
    }

    //format the string to request it from AWS
    private string Format_Request(string game_code)
    {
        string format_code = "https://s3.amazonaws.com/public.arcampaigns.com/games/" + game_code + ".json";
        return format_code;
    }

    //Request JSON from S3
    public void Request(string URL)
    {

        ui_script.Status("Requesting data from server...");

        WWW request = new WWW(URL);

        StartCoroutine(OnResponse(request));
    }

    //Wait for JSON to load
    private IEnumerator OnResponse(WWW req)
    {
        yield return req;

        //Update Status
        ui_script.Status("Requesting data from server.");

        if (req.text.Contains("Denied"))
        {
            ui_script.Status("Incorrect code or game does not exist.");
            StartCoroutine(ui_script.Restart_UI());
        }
        else
        {
            Process(req.text);
        }

    }

    //************
    //Process Downloaded Data
    //************

    //Save the downloaded data
    public void Save(string path, string game_code, string game_data)
    {
        string save_path = path + "/Games/" + game_code + ".json";

        if(game_script.Get_Device() == "android")
        {
            save_path = path + "/" + game_code + ".json";
        }

        File.WriteAllText(save_path, game_data);
    }

    //*** Processing downloaded data ***//

    //Process the downlaoded data
    private void Process(string game_data)
    {
        ui_script.Status("Saving Game Data...");

        //Saving to local memory
        try
        {
            Save(game_script.Get_Directory(), game_script.Get(), game_data);
            ui_script.Status("Loading Game...");

            //Change the scene
            ui_script.Change_Scene();

        }
        catch (Exception e)
        {
            ui_script.Status("Error Saving the game");

            StartCoroutine(ui_script.Restart_UI());
        }


    }

    //Delete saved game
    public void Delete()
    {
        string save_path = game_script.Get_Directory() +"/Games/" + game_script.Get() + ".json";

        if (game_script.Get_Device() == "android")
        {
            save_path = game_script.Get_Directory() + "/" + game_script.Get() + ".json";
        }

        File.Delete(save_path);

        StartCoroutine(ui_script.Restart_UI());
    }

    //************
    //Managing stored data
    //************

    //Check if the Game is already on the device
    public bool Check_Local()
    {
        //bool for saved data file
        bool isSaved = false;

        //formatted string of data path
        string save_path = game_script.Get_Directory() + "/Games/" + game_script.Get() + ".json";

        //Adjust for mobile
        if(game_script.Get_Device() == "android")
        {
            save_path = game_script.Get_Directory() + "/" + game_script.Get() + ".json";
        }

        if (File.Exists(save_path))
        {
            isSaved = true;
        }

        return isSaved;
    }


    //************
    //Main Entry Point
    //************

    public void Run()
    {
        //Check if the data exists locally

        if (Check_Local())
        {
            StartCoroutine(ui_script.Saved_Game());
        }
        else
        {
            //If data is not local, download from server
            Request(Format_Request(game_script.Get()));
        }


    }

    //************
    //LifeCycle
    //************
    private void Awake()
    {
        //Load script classes
        game_script = GetComponent<Game>();

        ui_script = GetComponent<UI>();
    }


    //************
    //Debug Only
    //************
    public Text debug_message;

    public bool debug_mode = true;

    private void Set_Debug(string message)
    {
        debug_message.text = message;
    }

 
}
