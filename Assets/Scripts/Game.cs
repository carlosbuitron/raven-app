﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{

    //************
    //Logic Variables
    //************

    public static string game_code = "default";
    public static bool update = false;
    public static string directory;
    public static string device;

    //************
    //Methods
    //************

    //Get the master game code
    public string Get()
    {
        return game_code;
    }

    //Set the master game code
    public void Set(string game_code_update)
    {
        game_code = game_code_update;
    }

    //Get the status of update bool
    public bool Get_Update()
    {
        return update;
    }

    //Set the status of the update bool
    public void Set_Update(bool update_status)
    {
        update = update_status;
    }

    public void Check_Device()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            print("iOS Device");
            device = "apple";
            directory = Application.dataPath + "/Raw/";
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            print("Android Device");
            device = "android";
            //directory = "jar:file://" + Application.dataPath + "!/assets/";
            directory = Application.persistentDataPath;
        }
        else
        {
            print("Desktop or Laptop");
            device = "desktop";
            directory = Application.dataPath + "/StreamingAssets/";
        }
    }

    //Get what type of device the app is running
    public string Get_Device()
    {
        return device;
    }

    //Get the directory to save downloads
    public string Get_Directory()
    {
        return directory;
    }

    //************
    //LifeCycle
    //************

    private void Awake()
    {
        //Check the type of device
        Check_Device();

    }

}
