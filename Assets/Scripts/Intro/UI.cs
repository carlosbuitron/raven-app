﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    //************
    //Logic Variables
    //************
    string game_code;

    string status_message;

    public InputField code_search;

    public Text status;

    public GameObject Input_UI;

    public GameObject Saved_UI;


    //************
    //Classes
    //************
    Game game_script;
    Network network_script;

    //************
    //Methods
    //************


    //************
    //Default Game
    //************

    //load the default game mode
    public void Load()
    {
        print("Ready to load default settings.");
        print(game_script.Get());

        Change_Scene();
    }


    //************
    //Intro UI
    //************

    //search for a custom raven code
    public void Search()
    {
        //start searching status
        Status("Searching for game...");

        //Hiding the UI while the search is running
        Toggle_UI(false);

        //store the value of the input field
        string code_search_text = code_search.text;

        //make sure the input field is not empty
        if (string.IsNullOrEmpty(code_search_text))
        {
            //Showing the UI when there is no code entered
            Toggle_UI(true);
            Status("Please enter a valid\nRaven code.");
            return;
        }

        game_script.Set(code_search.text);

        Status("Searching...");

        //Check for local vs online version

        print(game_script.Get());

        //Turn on the saved Game UI
        //StartCoroutine(Saved_Game());
        network_script.Run();

    }



    //************
    //Saved Game UI
    //************

    private void Toggle_UI_Saved(bool setting)
    {
        Saved_UI.SetActive(setting);
    }

    //Load the Saved Game UI
    public IEnumerator Saved_Game()
    {
        yield return new WaitForSeconds(1);
        Status("");
        Toggle_UI_Saved(true);
    }

    //Update and Load Game
    public void Update_Game()
    {
        network_script.update_load();
    }

    //Delete Game
    public void Delete_Game()
    {
        Toggle_UI_Saved(false);
        Status("Deleting game, please wait...");

        network_script.Delete();

    }


    //************
    //UI Helpers
    //************

    //Restart the UI
    public IEnumerator Restart_UI()
    {
        yield return new WaitForSeconds(1);
        Toggle_UI(true);
        StartCoroutine(Reset_Status());

        //Reset the game code
        game_script.Set("default");

        //Reset the input text
        code_search.text = "";
    }

    //Show saved UI if game is present
    private void Toggle_UI(bool setting)
    {
        Input_UI.SetActive(setting);
    }

    //************
    //Status Message Helpers
    //************

    //update the visible status message
    public void Status(string status_update)
    {
        status.text = status_update;
    }

    //Reset the Status after a small wait
    IEnumerator Reset_Status()
    {
        yield return new WaitForSeconds(1);
        Status("");
    }


    //************
    //Scene Manage
    //************

    //Change scene
    public void Change_Scene()
    {
        SceneManager.LoadScene("Scenes/render");
    }

    //************
    //LifeCycle
    //************

    private void Awake()
    {
        //Load script classes
        game_script = GetComponent<Game>();
        network_script = GetComponent<Network>();
    }

    private void Start()
    {
        //Debug Settings
        Check_Debug();
    }

    //************
    //Debug Only
    //************
    public Text debug_message;

    public bool debug_mode = true;

    private void Set_Debug(string message)
    {
        debug_message.text = message;
    }

    private void Check_Debug()
    {

        if (debug_mode)
        {
            print(game_script.Get_Device());
            Set_Debug(game_script.Get_Device());
        }
        else
        {
            Set_Debug("");
        }

    }

}
